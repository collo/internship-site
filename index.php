<?php

session_start();

require_once 'vendor/autoload.php';
require_once 'vendor/slim/extras/Slim/Extras/Views/Twig.php';

$app = new \Slim\Slim();

$app->config('templates.path', 'templates');
$app->config('debug', 'true');

\Slim\Extras\Views\Twig::$twigExtensions = array(
    'Twig_Extensions_Slim',
);

$app->view(new \Slim\Extras\Views\Twig());

function helloworld(){
	$app = \Slim\Slim::getInstance();
	$app->render('helloworld.twig.html');
}

function welcome(){
    $app = \Slim\Slim::getInstance();
    $app->render('welcome.twig.html');
}

function downloads(){
    $app = \Slim\Slim::getInstance();
    $app->render('downloads.twig.html');
}

function profiles(){
    $app = \Slim\Slim::getInstance();
    $app->render('student_profiles.twig.html');
}

function profile($name){
    $app = \Slim\Slim::getInstance();
    $student -> name = $name;
    $app->view()->appendData(array("student"=>$student));
    $app->render('student_profile.twig.html');
}

function student_signup(){
    $app = \Slim\Slim::getInstance();
    $app->render('student_signup.twig.html');
}

$app->get('/signup/student', 'student_signup');
$app->get('/profile/:name', 'profile');
$app->get('/profiles', 'profiles');
$app->get('/downloads', 'downloads');
$app->get('/hello','helloworld');
$app->get('/', 'welcome');

$app->run();

?>
