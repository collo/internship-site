# SCI Internship Site

## Contributing

Have these installed:

* Have git installed
* Have a [bitbucket](https://bitbucket.org) account. 
* Make sure your webserver works with ```.htaccess```. Wamp and Xampp should already be capable of doing this so you don't need to change a thing.
* All the code is in the ```index.php``` file. We might need to refactor it later but for now it's just dirty and quick.
* Changes to the code will be by forks and pull requests.
* Make all pull requests to the develop branch ie **All** developing is to happen on the **develop** branch. The master branch will hold the actual active site.

**Code Away!**
